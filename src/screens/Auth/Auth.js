import React, { Component } from 'react';
import { View, Text, Button, TextInput, StyleSheet, ImageBackground } from 'react-native';

import HeadingText from '../../components/UI/HeadingText/HeadingText';
import DefaultInput from '../../components/UI/DefaultInput/DefaultInput';
import MainText from '../../components/UI/MainText/MainText';
import ButtonWithBackground from '../../components/UI/ButtonWithBackground/ButtonWithBackground';
import backgroundImage from '../../assets/big-one.png'

import startMainTabs from '../MainTabs/startMainTabs';

class AuthScreen extends Component {

  loginHanlder = () => {
    startMainTabs();
  }

  render() {
    return (
      <ImageBackground source={backgroundImage} style={styles.backgroundImage}>
        <View style={styles.container}>
          <MainText>
            <HeadingText style={styles.heading}>Please Log IN</HeadingText>
          </MainText>
          <ButtonWithBackground color="#29aaf4">Switch to Login</ButtonWithBackground>
          <View style={styles.inputContainer}>
            <DefaultInput placeholder="E-mail"/>
            <DefaultInput placeholder="Password"/>
            <DefaultInput placeholder="Confirm Password"/>
          </View>
          <ButtonWithBackground onPress={this.loginHanlder} color="#29aaf4">Submit</ButtonWithBackground>
        </View>
      </ImageBackground>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  textHeading: {
    fontSize: 28,
    fontWeight: 'bold'
  },
  heading: {
    color: 'white'
  },
  inputContainer: {
    width: '80%',
    marginTop: 10
  },
  backgroundImage: {
    width: '100%',
    flex: 1
  }
});

export default AuthScreen;
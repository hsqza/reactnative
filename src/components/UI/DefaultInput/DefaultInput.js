import React from 'react';
import { TextInput, StyleSheet } from 'react-native';

const defaultInput = props => (
  <TextInput {...props} style={[styles.input, props.style]}  />
);

const styles = StyleSheet.create({
  input: {
    width: '100%',
    borderColor: '#ccc',
    backgroundColor: '#ddd',
    borderBottomWidth: 1,
    padding: 5,
    marginBottom: 8
  }
})

export default defaultInput;